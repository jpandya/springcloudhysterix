package com.jiten.innovations.springCloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jiten.innovations.springCloud.service.ServiceClass;

@RestController
public class DemoController {
	
	@Autowired
	ServiceClass serviceClass;
	
	@GetMapping("/getMessage")
	public String getMsg() {
		return serviceClass.getMsg();
	}
}
