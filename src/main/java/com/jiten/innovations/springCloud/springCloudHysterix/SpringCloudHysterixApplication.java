package com.jiten.innovations.springCloud.springCloudHysterix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@EnableHystrixDashboard
@EnableCircuitBreaker
@EnableFeignClients(basePackageClasses = com.jiten.innovations.springCloud.feignClients.FeignClientDependentMicroServiceClient.class)
@ComponentScan(basePackages = {"com.jiten.innovations.springCloud.controller","com.jiten.innovations.springCloud.fallback",
							   "com.jiten.innovations.springCloud.config","com.jiten.innovations.springCloud.service"})
@SpringBootApplication
public class SpringCloudHysterixApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudHysterixApplication.class, args);
	}

}
