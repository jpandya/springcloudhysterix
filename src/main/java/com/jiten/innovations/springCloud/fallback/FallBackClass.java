package com.jiten.innovations.springCloud.fallback;

import org.springframework.stereotype.Component;

@Component
public class FallBackClass {
	public String getMsg() {
		return "Hello from FallBackClass class";
	}
}
