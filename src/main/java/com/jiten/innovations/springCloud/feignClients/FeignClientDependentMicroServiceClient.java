package com.jiten.innovations.springCloud.feignClients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.jiten.innovations.springCloud.fallback.FallBackClass;

@FeignClient(name="FeignClientDependentMicroServiceDemo",url="localhost:9002",fallback = FallBackClass.class)
public interface FeignClientDependentMicroServiceClient {
	
	@GetMapping("/msg")
	public String getMsg();
	
}
