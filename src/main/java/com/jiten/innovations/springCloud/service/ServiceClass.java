package com.jiten.innovations.springCloud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import com.jiten.innovations.springCloud.feignClients.FeignClientDependentMicroServiceClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class ServiceClass {

	@Autowired
	private RestTemplate restTemplate;

	@HystrixCommand(fallbackMethod="getDefaultMessage") public String getMsg() {
	  String message; 
	  ResponseEntity<String> responseEntity = restTemplate.getForEntity("http://localhost:9002/msg", String.class);
	  responseEntity = null;
	  if(responseEntity.getStatusCode() == HttpStatus.OK) { 
		  message = responseEntity.getBody(); 
	  }else { 
		  message = null; 
	  }
	  return message;
	  }
	  
	  

	public String getDefaultMessage() {
		return "Default Message";
	}
	 
	
	/*
	@Autowired
	FeignClientDependentMicroServiceClient client;

	@GetMapping("/open-feign-client/getMessage")
	public String getMsg() {
		return client.getMsg();
	}
	*/
}
